package main.java.exception;

public class DataImportException extends RuntimeException {

    public DataImportException(String message) {
        super(message);
    }
}
