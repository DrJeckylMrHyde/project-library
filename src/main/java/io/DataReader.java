package main.java.io;

import main.java.model.Book;
import main.java.model.LibraryUser;
import main.java.model.Magazine;

import java.util.Scanner;

public class DataReader {

    private Scanner sc = new Scanner(System.in);
    private ConsolePrinter printer;

    public DataReader(ConsolePrinter printer) {

        this.printer = printer;
    }

    public String getString() {

        return sc.nextLine();
    }

    public int getInt() {
        try {
            return sc.nextInt();
        } finally {
            sc.nextLine();
        }
    }

    public void close() {

        sc.close();
    }

    public LibraryUser createLibraryUser() {
        printer.printLine("Name");
        String firstName = sc.nextLine();
        printer.printLine("Surname");
        String lastName = sc.nextLine();
        printer.printLine("Pesel");
        String pesel = sc.nextLine();
        return new LibraryUser(firstName, lastName, pesel);
    }

    public Book readAndCreateBook() {
        System.out.println("Title:");
        String title = sc.nextLine();
        System.out.println("Publisher:");
        String publisher = sc.nextLine();
        System.out.println("Release date:");
        int year = getInt();
        System.out.println("Author:");
        String author = sc.nextLine();
        System.out.println("Pages:");
        int pages = getInt();
        System.out.println("ISBN:");
        String isbn = sc.nextLine();
        return new Book(title, publisher, year, author, pages, isbn);
    }

    public Magazine readAndCreateMagazine() {
        System.out.println("Title:");
        String title = sc.nextLine();
        System.out.println("Publisher:");
        String publisher = sc.nextLine();
        System.out.println("Release year:");
        int year = getInt();
        System.out.println("Release month");
        int month = getInt();
        System.out.println("Release day");
        int day = getInt();
        System.out.println("Language:");
        String language = sc.nextLine();
        return new Magazine(title, publisher, year, month, day, language);
    }
}
