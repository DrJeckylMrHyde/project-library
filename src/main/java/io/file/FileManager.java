package main.java.io.file;

import main.java.model.Library;

public interface FileManager {

    Library importData();

    void exportData(Library library);
}
