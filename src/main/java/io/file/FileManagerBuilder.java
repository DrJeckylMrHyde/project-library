package main.java.io.file;

import main.java.exception.NoSuchFileTypeException;
import main.java.io.ConsolePrinter;
import main.java.io.DataReader;

public class FileManagerBuilder {

    private ConsolePrinter printer;
    private DataReader reader;

    public FileManagerBuilder(ConsolePrinter printer, DataReader reader) {
        this.printer = printer;
        this.reader = reader;
    }

    public FileManager build(){
        printer.printLine("Choose data format:");
        FileType fileType = getFileType();
        switch (fileType){
            case SERIAL:
                return new SerializableFileManager();
            case CSV:
                return new CsvFileManager();
            default:
                throw new NoSuchFileTypeException("Unhandled data format");
        }
    }

    private FileType getFileType() {
        boolean typeOK = false;
        FileType result = null;
        do{
            printTypes();
            String type = reader.getString().toUpperCase();
            try{
                result = FileType.valueOf(type);
                typeOK = true;
            } catch (IllegalArgumentException e){
                printer.printLine("Unhandled data format, choose again.");
            }
        }while (!typeOK);

        return result;
    }

    private void printTypes() {
        for (FileType value : FileType.values()) {
            printer.printLine(value.name());
        }
    }
}
