package main.java.model.comparator;

import main.java.model.Publication;

import java.time.Year;
import java.util.Comparator;

public class DateComparator implements Comparator<Publication> {

    @Override
    public int compare(Publication p1, Publication p2) {
        if(p1 == null && p2 == null){
            return 0;
        } else if(p1 == null){
            return 1;
        } else if(p2 == null){
            return -1;
        }
        Year y1 = p1.getYear();
        Year y2 = p2.getYear();

        return -y1.compareTo(y2);
    }
}
