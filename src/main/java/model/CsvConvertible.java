package main.java.model;

public interface CsvConvertible {
    String toCsv();
}
