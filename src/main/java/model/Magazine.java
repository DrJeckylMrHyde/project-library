package main.java.model;

import java.time.MonthDay;
import java.util.Objects;

public class Magazine extends Publication {

    public static final String TYPE = "Magazine";

    private MonthDay monthDay;
    private String language;

    public Magazine(String title, String publisher, int year, int month, int day, String language) {
        super(title, publisher, year);
        this.language = language;
        this.monthDay = MonthDay.of(month, day);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + monthDay.getMonthValue() +
                ", " + monthDay.getDayOfMonth() + ", " + language;
    }

    @Override
    public String toCsv() {
        return (TYPE + ";") +
                getTitle() + ";" +
                getPublisher() + ";" +
                getYear() + ";" +
                monthDay.getMonthValue() + ";" +
                monthDay.getDayOfMonth() + ";" +
                language + "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return Objects.equals(monthDay, magazine.monthDay) &&
                Objects.equals(language, magazine.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), monthDay, language);
    }
}
