package main.java.app;

import main.java.exception.*;
import main.java.io.ConsolePrinter;
import main.java.io.DataReader;
import main.java.io.file.FileManager;
import main.java.io.file.FileManagerBuilder;
import main.java.model.*;

import java.util.Comparator;
import java.util.InputMismatchException;

class LibraryControl {


    private ConsolePrinter printer = new ConsolePrinter();

    private DataReader dataReader = new DataReader(printer);

    private FileManager fileManager;

    private Library library;

    LibraryControl() {
        fileManager = new FileManagerBuilder(printer, dataReader).build();
        try {
            library = fileManager.importData();
            printer.printLine("Data imported from file");
        } catch (DataImportException | InvalidDataException e) {
            printer.printLine(e.getMessage());
            printer.printLine("New base initialized");
            library = new Library();
        }
    }

    void controlLoop() {
        Option option;

        do {
            printOptions();
            option = getOption();
            switch (option) {
                case EXIT:
                    exit();
                    break;
                case ADD_BOOK:
                    addBook();
                    break;
                case ADD_MAGAZINE:
                    addMagazine();
                    break;
                case PRINT_BOOKS:
                    printBooks();
                    break;
                case PRINT_MAGAZINES:
                    printMagazines();
                    break;
                case DELETE_BOOK:
                    deleteBook();
                    break;
                case DELETE_MAGAZINE:
                    deleteMagazine();
                    break;
                case ADD_USER:
                    addUser();
                    break;
                case PRINT_USERS:
                    printUsers();
                    break;
                case DELETE_USER:
                    deleteUser();
                    break;
                case FIND_BOOK:
                    findBook();
                    break;
                default:
                    printer.printLine("Nie ma takiej opcji, wprowadź ponownie:");
            }
        } while (option != Option.EXIT);
    }

    private void findBook() {
        printer.printLine("Write the title of the book");
        String title = dataReader.getString();
        String notFoundMessage = "There' s no such book";
        library.findPublicationByTitle(title)
                .map(Publication::toString)
                .ifPresent(System.out::println);
    }

    private void deleteUser() {
        try {
            LibraryUser user = dataReader.createLibraryUser();
            if (library.removeUser(user)) {
                printer.printLine("User deleted");
            } else {
                printer.printLine("There is no such user in library to remove");
            }
        } catch (InputMismatchException e) {
            printer.printLine("It was unable to create magazine");
        }
    }

    private void deleteMagazine() {
        try {
            Magazine magazine = dataReader.readAndCreateMagazine();
            if (library.removePublication(magazine)) {
                printer.printLine("Magazine deleted");
            } else {
                printer.printLine("There is no such magazine in library to remove");
            }
        } catch (InputMismatchException e) {
            printer.printLine("It was unable to create magazine");
        }
    }

    private void deleteBook() {
        try {
            Book book = dataReader.readAndCreateBook();
            if (library.removePublication(book)) {
                printer.printLine("Book deleted");
            } else {
                printer.printLine("There is no such book in library to remove");
            }
        } catch (InputMismatchException e) {
            printer.printLine("It was unable to create book");
        }
    }

    private Option getOption() {
        boolean optionOK = false;
        Option option = null;
        while (!optionOK) {
            try {
                option = Option.createFromInt(dataReader.getInt());
                optionOK = true;
            } catch (NoSuchOptionException e) {
                printer.printLine(e.getMessage() + ", try again:");
            } catch (InputMismatchException e) {
                printer.printLine("wrong value for option, try again");
            }
        }
        return option;
    }


    private void printOptions() {
        for (Option option : Option.values()) {
            System.out.println(option);
        }
        printer.printLine("Please choose action:");
    }

    private void addBook() {
        try {
            Book book = dataReader.readAndCreateBook();
            library.addPublication(book);
        } catch (InputMismatchException e) {
            printer.printLine("Book adding failure, wrong data");
        } catch (ArrayIndexOutOfBoundsException e) {
            printer.printLine("You have reached limit of publications in library");
        }
    }

    private void addMagazine() {
        try {
            Magazine magazine = dataReader.readAndCreateMagazine();
            library.addPublication(magazine);
        } catch (InputMismatchException e) {
            printer.printLine("Magazine adding failure, wrong data");
        } catch (ArrayIndexOutOfBoundsException e) {
            printer.printLine("You have reached limit of publications in library");
        }
    }

    private void addUser() {
        LibraryUser libraryUser = dataReader.createLibraryUser();
        try {
            library.addUser(libraryUser);
        } catch (UserAlreadyExistsException e) {
            printer.printLine(e.getMessage());
        }
    }

    private void printBooks() {
        printer.printBooks(library.getSortedPublications(
                Comparator.comparing(Publication::getTitle,String.CASE_INSENSITIVE_ORDER)
//                (p1, p2) -> p1.getTitle().compareToIgnoreCase(p2.getTitle())
        ));
    }

    private void printMagazines() {
        printer.printMagazines(library.getSortedPublications(
                Comparator.comparing(Publication::getTitle,String.CASE_INSENSITIVE_ORDER)
//                (p1, p2) -> p1.getTitle().compareToIgnoreCase(p2.getTitle())
        ));
    }

    private void printUsers() {

        printer.printUsers(library.getSortedUsers(
                Comparator.comparing(User::getLastName,String.CASE_INSENSITIVE_ORDER)));
//                (lu1, lu2) -> lu1.getLastName().compareToIgnoreCase(lu2.getLastName())));
    }

    private void exit() {
        try {
            fileManager.exportData(library);
            printer.printLine("Data export to file finished succesfully");
        } catch (DataExportException e) {
            printer.printLine(e.getMessage());
        }
        dataReader.close();
        System.out.println("End of work");
    }

    private enum Option {

        EXIT(0, "close application"),
        ADD_BOOK(1, "Add new Book"),
        ADD_MAGAZINE(2, "add new Magazine"),
        PRINT_BOOKS(3, "show all availablee books"),
        PRINT_MAGAZINES(4, "show all availablee magazines"),
        DELETE_BOOK(5, "Delete chosen book"),
        DELETE_MAGAZINE(6, "Delete chosen magazine"),
        ADD_USER(7, "add new LibraryUser"),
        PRINT_USERS(8, "show all LibraryUsers"),
        DELETE_USER(9, "Delete chosen user"),
        FIND_BOOK(10,"Find book by title");

        private int value;
        private String description;

        Option(int value, String description) {
            this.value = value;
            this.description = description;
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }

        static Option createFromInt(int option) throws NoSuchOptionException {
            try {
                return Option.values()[option];
            } catch (ArrayIndexOutOfBoundsException e) {
                throw new NoSuchOptionException("There' s no such option in library");
            }
        }
    }
}
