package main.java.app;

public class LibraryApp {
    private static final String appName = "Library v1.9";

    public static void main(String[] args) {
        LibraryControl libraryControl = new LibraryControl();

        System.out.println(appName);

        libraryControl.controlLoop();


    }
}
